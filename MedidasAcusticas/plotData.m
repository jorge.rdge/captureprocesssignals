function [  ] = plotData( data )
%PLOTDATA Summary of this function goes here
%   Detailed explanation goes here


figure;
for k = 1:length(data)
    if data(k).anchuras_current(end) < 1e-6
        plot(data(k).vhList, data(k).anchuras_current/1e-9); hold on;
    else
        plot(data(k).vhList, data(k).anchuras_current); hold on;
    end
end

xlabel('High Voltage Input [V]');
ylabel('Current Pulse Width [ns]')
title('Voltage Input vs current pulse width')
legend(num2str(([data.dataWidth])', '%d ns'))


figure;
for k = 1:length(data)
    plot(data(k).vhList, data(k).opticalPulseWidth/1e-9); hold on;
end

xlabel('High Voltage Input [V]');
ylabel('Optical Pulse Width [ns]');
title('Voltage Input vs optical pulse width')
legend(num2str(([data.dataWidth])', '%d ns'))

figure;
for k = 1:length(data)
    plot(data(k).vhList, data(k).maxCurr*10); hold on;
end

xlabel('High Voltage Input [V]');
ylabel('Current Pulse amplitude [A]')
title('Voltage Input vs current pulse amplitude')
legend(num2str(([data.dataWidth])', '%d ns'))

% 
% figure;
% plot(data.vhList, data.opticalPulseAmplitude/1e-3)
% xlabel('High Voltage Input [V]');
% ylabel('Optical Pulse Amplitude [mV]')
% title(num2str(data.dataWidth, 'Voltage Input vs optical pulse amplitude for %d ns input'))

figure;
for k = 1:length(data)
    plot(data(k).vhList, data(k).PpList); hold on;
end

xlabel('High Voltage Input [V]');
ylabel('Peak Power [W]')
title('Voltage Input vs optical pulse peak Power')
legend(num2str(([data.dataWidth])', '%d ns'))


figure;
for k = 1:length(data)
    plot(data(k).vhList, data(k).meanPower); hold on;
end

xlabel('High Voltage Input [V]');
ylabel('Energy per pulse [uJ]')
title('Voltage Input vs optical pulse energy')
legend(num2str(([data.dataWidth])', '%d ns'))


end

