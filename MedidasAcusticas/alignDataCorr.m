function [ t, t2 ] = alignDataCorr( data, criteria )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
ref = normalizado(data(end).acousticPulse30AAverage.signal);
for k = 1:length(data)
  tmpsig = normalizado(data(k).acousticPulse30AAverage.signal);
  [tmpC, tmpL] = xcorr(tmpsig, ref);
  [~, idx] = max(tmpC);
  t(k) = tmpL(idx);
  
  [~, idxMax] = max(tmpsig);
  for c = idxMax:-1:1
    if tmpsig(c) <= criteria
        t2(k) = c;
        break;
    end
  end
  
end

end

