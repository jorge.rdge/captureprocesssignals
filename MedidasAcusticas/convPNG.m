close all; clear all;


rutaCapturasAcoustic = '.\Figures\';

dataListDir = ls(rutaCapturasAcoustic);
% Figures
% 3:12 --> 20A
% 13:22 --> 20A FFT
% 23:32 --> 30A
% 33:42 --> 30A FFT
% 43:52 --> WAT y 10MHZ
% 53:62 --> WAT y 10MHZ FFT
% 63:end --> no hace falta zoom

% Figures\figuresWat13
dataListTmp = dataListDir(end-10:end-2, :);

rutaCapturasAcousticSave = '.\Figures\PNG\COMP\';
for k = 1:size(dataListTmp, 1)
    h = openfig([rutaCapturasAcoustic, dataListTmp(k, :)], 'new');
    saveas(h, [rutaCapturasAcousticSave, dataListTmp(k, 1:end-5)], 'png');
%     xlim([2.194, 2.542])
%     saveas(h, [rutaCapturasAcousticSave, dataListTmp(k,1:end-9),'_ZOOM'], 'png');
    close all;
end