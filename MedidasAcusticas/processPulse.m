function [ posWidth, negWidth, posNegWidth, maxVal, risetime] = ...
            processPulse( t, signal, idx, plotOpt, pulseWidth, pulseOpt )
        
    signalWin = signal(idx); tWin = t(idx);
    [maxVal, maxIdx] = max(signalWin); [minVal, minIdx] = min(signalWin);

    signalWinNorm = zeros(size(signalWin));
    for k = 1:length(signalWin)
        if signalWin(k) < 0
            signalWinNorm(k) = signalWin(k)/abs(minVal);
        else
            signalWinNorm(k) = signalWin(k)/maxVal;
        end
    end

    posNegWidth = abs(tWin(minIdx) - tWin(maxIdx));
    [posWidth, pos1, pos2, risetime ]= ancho_pulso(tWin, signalWinNorm, 0.5);
    [negWidth, neg1, neg2, ~] = ancho_pulso(tWin, -signalWinNorm, 0.5);
    
    if plotOpt
        if strcmp(pulseOpt, 'Acoustic')
            figure;
            hold on;
            plot(tWin(pos1), signalWin(pos1), 'r*',tWin(neg2), signalWin(neg2), 'k*')             
            plot(tWin(maxIdx), maxVal, 'y*', tWin(minIdx), minVal, 'y*')
            plot(tWin, signalWin); 
            plot(tWin(neg1), signalWin(neg1), 'k*', tWin(pos2), signalWin(pos2), 'r*')
            hold off;

            xlabel('s');ylabel('V');title(num2str(pulseWidth,'%d ns'));
            legend({['Positive Width ', num2str(posWidth/1e-9, '%0.2f ns')],...
                    ['Negative Width ', num2str(negWidth/1e-9, '%0.2f ns')],...
                    ['Positive-Negative Width ', num2str(posNegWidth/1e-9, '%0.2f ns')]})
        else
            figure;
            hold on;
            plot(tWin(pos1), signalWin(pos1), 'r*',tWin(pos2), signalWin(pos2), 'r*')             
            plot(tWin, signalWin); 
            hold off;

            xlabel('s');ylabel('V');title(num2str(pulseWidth,'%d Vh'));
            legend({['Positive Width ', num2str(posWidth/1e-9, '%0.2f ns')],...
                    ['Maximmum Value ', num2str(maxVal, '%0.2f V')]})
        end
        
    end
end

