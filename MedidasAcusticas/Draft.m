%%
%% Import and Order Data
rutaCapturasAcoustic = '.\Medidas\';

dataListDir = ls(rutaCapturasAcoustic);
dataListTmp = dataListDir(3:end-2, :);

% 1: 10MHz 2: 10MHz Zoom 3: 25MHz 4:25MHz Zoom
load([rutaCapturasAcoustic, dataListTmp(1, :)]);
vh20A = [50, 40, 35, 32.5, 30, 30, 30, 30, 30, 30];
vh30A = [60, 55, 47.5, 47.5, 47.5, 45, 45, 45, 45, 45];
dataWidthList = 10:10:100;
%%
sig = normalizado(data(3).acousticPulse30AAverage.signal);

time_acoustic = data(3).acousticPulse30AAverage.t0 + data(3).acousticPulse30AAverage.dt*...
                (0:length(data(3).acousticPulse30AAverage.signal) - 1);              

plot(sig)

maxph = 0.10;
minph = 0.10;
[peakMax, idMax] = findpeaks(sig, 'MinPeakHeight',maxph, 'MinPeakDistance', 150);
[peakMin, idMin] = findpeaks(-sig, 'MinPeakHeight',minph, 'MinPeakDistance', 150);

for k = idMax(1):-1:1
    if sig(k) <= 0
        idTrig = k; valTrig = sig(k);
        break;
    end
end
for k = idMax(2):-1:1
    if sig(k) <= 0
        idStart = k; valStart = sig(k);
        break;
    end
end
figure;
plot(time_acoustic, sig); hold on;
plot(time_acoustic(idMax), peakMax, 'r*');
plot(time_acoustic(idMin), -peakMin, 'b*');
plot(time_acoustic(idTrig), valTrig, 'y*');
plot(time_acoustic(idStart), valStart, 'y*');

c = 1470; % m/s;

tstart = time_acoustic(idStart); ttrig = time_acoustic(idTrig);
tmax = time_acoustic(idMax);
tmin = time_acoustic(idMin);

xstart = tstart*c/1e-3;
xmax = tmax*c/1e-3;
xmin = tmin*c/1e-3;

dMax = [xmax(2)-xmax(1), xmax(4) - xmax(2)]%, xmax(4) - xmax(3)]
dMin = [xmin(2) - xmin(1), xmin(4) - xmin(2)]%, xmin(4) - xmin(3)]

dStart = (tstart - ttrig)*c/1e-3
