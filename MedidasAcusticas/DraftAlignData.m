sig1 = (data(1).acousticPulse30AAverage.signal);
sig2 = (data(5).acousticPulse30AAverage.signal);

sig1Norm = normalizado(data(1).acousticPulse30AAverage.signal);
sig2Norm = normalizado(data(5).acousticPulse30AAverage.signal);


figure;plot(sig1); hold on; plot(sig2)


[alList, idx] = alignData(data, 0.02);

for k = 1:length(data)
    plot(alList(k, :)); hold on;
    a(k) = alList(k, idx(k));
end

figure;    plot(circshift(alList(1, :)', [100,0])); hold on;plot(alList(7, :)); 

[t1, t2] = alignDataCorr(data, 1);

for k = 1:length(data)
    sig = (smooth(circshift(data(k).acousticPulse30AAverage.signal, -abs(t1(k))), 5));
    plot(sig); hold on;
end
