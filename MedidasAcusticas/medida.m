
clear all
close all

hOsc=ds2000;

nSaves = 16;
autoScale = 0;

% hFig = figure;
% hAxes(1) = subplot(1,1,1);

%hOsc.saveMode = 'fullframe';
%hOsc.plotMode = 'fullframe';

hOsc.saveMode = 'preview';
hOsc.plotMode = 'preview';

hOsc.stopAfterSave = 1;
pulseWidthList = 10:10:100;

%%
for k = 1:length(pulseWidthList)
%     hOsc.acquire.mode = 'Sampled';
    % hOsc.trigger.sweep = 'normal';

%     hOsc.channel(1).acquire = 1;
%     hOsc.channel(1).line = line('color', 'y');
%     hOsc.channel(1).save = nSaves;
%     hOsc.channel(1).autoScale = autoScale;
%     hOsc.channel(1).display=1;

    hOsc.acquire.mode = 'Sampled';
    hOsc.channel(2).acquire = 1;
    hOsc.channel(2).line = line('color', 'b');
    hOsc.channel(2).save = nSaves;
    hOsc.channel(2).autoScale = autoScale;
    hOsc.channel(2).display=1;


    hOsc.start;
    wait(hOsc);
%     data(k).currentPulse = hOsc.channel(1).signal;
    data(k).acousticPulse20A = hOsc.channel(2).signal;
    
%     hOsc.channel(1).save = 1;
    hOsc.channel(2).save = 1;
    hOsc.acquire.mode = 'Average';
    hOsc.acquire.nAverages = 16;

    hOsc.start;
    wait(hOsc);

%     data(k).currentPulseAverage = hOsc.channel(1).signal;
    data(k).acousticPulse20AAverage = hOsc.channel(2).signal;
    
    input('Now measure for 30A')

    
    hOsc.acquire.mode = 'Sampled';
    hOsc.channel(2).acquire = 1;
    hOsc.channel(2).line = line('color', 'b');
    hOsc.channel(2).save = nSaves;
    hOsc.channel(2).autoScale = autoScale;
    hOsc.channel(2).display=1;


    hOsc.start;
    wait(hOsc);
%     data(k).currentPulse = hOsc.channel(1).signal;
    data(k).acousticPulse30A = hOsc.channel(2).signal;
    
%     hOsc.channel(1).save = 1;
    hOsc.channel(2).save = 1;
    hOsc.acquire.mode = 'Average';
    hOsc.acquire.nAverages = 16;

    hOsc.start;
    wait(hOsc);

%     data(k).currentPulseAverage = hOsc.channel(1).signal;
    data(k).acousticPulse30AAverage = hOsc.channel(2).signal;

    plot(data(k).acousticPulse20AAverage.signal)
    figure

    plot(data(k).acousticPulse30AAverage.signal)
    
    data(k).pulseWidth = pulseWidthList(k);
    cont = input(['Continue with measure ' num2str(pulseWidthList(k)+10, '%2d ns?')]);
    if isempty(cont); cont = 1;end;
    if cont; continue; close all; else break;end;
end


%%
 currDate = clock;
 dateStr = [];
 for k = 1:length(currDate)
     dateStr = [dateStr '-' num2str(round(currDate(k)))];
 end
 
save(['REPEAT60ns20A' dateStr, '_20dB_25MHz', '.mat'],'data');

hOsc.acquire.mode = 'Sampled';
hOsc.runAcquire;

delete(hOsc);
%%
figure;
for k = 8:length(data)
    sig = data(k).opticalPulseAverage.signal/max(data(k).opticalPulseAverage.signal);
%     fs = 2/data(k).opticalPulseAverage.dt;
%     sigtmp = lowpass(sig, 100e6, fs); %smooth(sig, 0.05, 'loess');
%     if k == 25; idx = 0;end
    sigtmp = smooth(sig, 10);
    plot(sigtmp); hold on;
end
%%
[val, idx] = min(data(k).opticalPulseAverage.signal);
figure;plot(circshift(data(k).opticalPulseAverage.signal, idx));
hold on; plot(data(k).opticalPulseAverage.signal)
