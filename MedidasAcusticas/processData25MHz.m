close all; 
clear all;

%% Import and Order Data
rutaCapturasAcoustic = '.\Medidas\';

dataListDir = ls(rutaCapturasAcoustic);
dataListTmp = dataListDir(3:end-2, :);

% 1: 10MHz 2: 10MHz Zoom 3: 25MHz 4:25MHz Zoom
load([rutaCapturasAcoustic, dataListTmp(4, :)]);
vh20A = [50, 40, 35, 32.5, 30, 30, 30, 30, 30, 30];
vh30A = [60, 55, 47.5, 47.5, 47.5, 45, 45, 45, 45, 45];
dataWidthList = 10:10:100;

%% Process acoustic 30 A

time_acoustic = data(1).acousticPulse30AAverage.t0 + data(1).acousticPulse30AAverage.dt*...
                (0:length(data(1).acousticPulse30AAverage.signal) - 1);              


plotOpt = 1;
posWidthListWat30A = zeros(length(dataWidthList), 1);
negWidthListWat30A = zeros(length(dataWidthList), 1);
posNegWidthListWat30A = zeros(length(dataWidthList), 1);
maxVal30A = zeros(length(dataWidthList), 1);
pulseOpt = 'Acoustic';

for k = 1:length(data)
[posWidthListWat30A(k), negWidthListWat30A(k), posNegWidthListWat30A(k), maxVal30A(k)] = ...
    processPulse(time_acoustic, ...
                 data(k).acousticPulse30AAverage.signal, ...
                 [1:length(data(k).acousticPulse30AAverage.signal)], ...
                 plotOpt, dataWidthList(k), pulseOpt);
end

sigList30 = zeros(10, 1400);
for k = 1:size(sigList30,1)
    sigList30(k, :) = smooth(data(k).acousticPulse20AAverage.signal, 15);
end

normOpt = 0; criteria = 0.02;
[alList30, idx] = alignData(sigList30, criteria, normOpt);

figure;
for k = 1:length(data)
    plot(time_acoustic/1e-6, alList30(k, :)); hold on;
end
legend(num2str(posWidthListWat30A/1e-9, '%0.2f ns'));

%% Process acoustic 20 A

time_acoustic = data(1).acousticPulse20AAverage.t0 + data(1).acousticPulse20AAverage.dt*...
                (0:length(data(1).acousticPulse20AAverage.signal) - 1);              


plotOpt = 1;
posWidthListWat20A = zeros(length(dataWidthList), 1);
negWidthListWat20A = zeros(length(dataWidthList), 1);
posNegWidthListWat20A = zeros(length(dataWidthList), 1);
maxVal20A = zeros(length(dataWidthList), 1);
pulseOpt = 'Acoustic';

for k = 1:length(data)
[posWidthListWat20A(k), negWidthListWat20A(k), posNegWidthListWat20A(k), maxVal20A(k)] = ...
    processPulse(time_acoustic, ...
                 data(k).acousticPulse20AAverage.signal, ...
                 [1:length(data(k).acousticPulse20AAverage.signal)], ...
                 plotOpt, dataWidthList(k), pulseOpt);
end

figure;
for k = 1:length(data)
    tmp = alignData(data(k).acousticPulse20AAverage.signal, 0, 200);
    plot(tmp(50:350));
    hold on;
end
legend(num2str(posWidthListWat20A/1e-9, '%0.2f ns'));

%% FFT acoustic 30A

time_acoustic = data(1).acousticPulse30AAverage.t0 + data(1).acousticPulse30AAverage.dt*...
                (0:length(data(1).acousticPulse30AAverage.signal) - 1);              
idxAcoustic  = 50:350;% 1:700 for 10MHz

figure;
for k = 1:length(data)
    plot(normalizado(data(k).acousticPulse20AAverage.signal(idxAcoustic)));
    hold on;
end


Ts = data(1).acousticPulse30AAverage.dt;
Fs = 1/Ts; Fn = Fs/2; 
pulseWidthList = 10:10:100;
legendCell = cellstr(num2str(pulseWidthList', '%-d ns'));

bwIdxList30A = zeros(length(data), 7);
bwList30A = zeros(size(bwIdxList30A));

range = 70e6;

for k = 1:length(data)
    tmp = alignData(data(k).acousticPulse20AAverage.signal, 0, 200);
    tmp = tmp(50:350);
%     tmp = data(k).acousticPulse30AAverage.signal(idxAcoustic);
    [fvec, S, bwIdxList30A(k, :) ] = spec(tmp, Fs, range);
    bwList30A(k,:) = fvec(bwIdxList30A(k,:))/1e6;
    
    figure;
    plot(fvec/1e6, 20*log10(abs(S)./max(abs(S)))); hold on;
    plot(fvec(bwIdxList30A(k, :))/1e6, [-6, -6, -10, -10,-20, -30, -40], 'r*')
    axis([0, range/1e6, -80, 2]);
    title(num2str(pulseWidthList(k), '%2.2f ns'));
    xlabel('MHz'); ylabel('dB')
% 
%     line(fvec, linspace(-20, -20, length(fvec)), 'Color', 'k');
%     line(fvec, linspace(-40, -40, length(fvec)), 'Color', 'k');
%     line(fvec, linspace(-60, -60, length(fvec)), 'Color', 'k');

end


%% FFT acoustic 20A

time_acoustic = data(1).acousticPulse20AAverage.t0 + data(1).acousticPulse20AAverage.dt*...
                (0:length(data(1).acousticPulse20AAverage.signal) - 1);              
idxAcoustic  = 1:350; % 1:700 for 10MHz
Ts = data(1).acousticPulse20AAverage.dt;
Fs = 1/Ts; Fn = Fs/2; 
pulseWidthList = 10:10:100;
legendCell = cellstr(num2str(pulseWidthList', '%-d ns'));
% for k = 1:size(data,1)
%     figure

%     subplot(2, 1,1)
%     plot(time_acoustic(idxListAcoustic{k}), ...
%             data(k).wat13.averaged.signal(idxListAcoustic{k})); 
%     hold on;
%     title(num2str(dataWidth(k)', '%-d ns'))
% 
%     subplot(2,1,2)
%     plotFreq(data(k).wat13.averaged.signal(idxListAcoustic{k}), Fs, 1e6, 70e6); hold on
% end
bwIdxList20A = zeros(length(data),7);
bwList20A = zeros(size(bwIdxList20A));

range = 70e6;

for k = 1:length(data)
    tmp = alignData(data(k).acousticPulse20AAverage.signal, 0, 200);
    tmp = tmp(50:350);
    
    [fvec, S, bwIdxList20A(k, :) ] = spec(tmp, Fs, range);

    bwList20A(k,:) = fvec(bwIdxList20A(k,:))/1e6;

    figure;
    plot(fvec/1e6, 20*log10(abs(S)./max(abs(S)))); hold on;
    plot(fvec(bwIdxList20A(k, :))/1e6, [-6, -6, -10, -10, -20, -30, -40], 'r*')
    axis([0, range/1e6, -80, 2]);

end

title('10MHz')
line(fvec, linspace(-20, -20, length(fvec)), 'Color', 'k');
line(fvec, linspace(-40, -40, length(fvec)), 'Color', 'k');
line(fvec, linspace(-60, -60, length(fvec)), 'Color', 'k');

xlabel('MHz'); ylabel('dB')
legend(legendCell);

%% Display Data

figure;
plot(pulseWidthList, posWidthListWat20A/1e-9); hold on
plot(pulseWidthList, posWidthListWat30A/1e-9)
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Pulse Width [ns]')
legend(['20A'], ['30A']);

figure;
plot(pulseWidthList, posNegWidthListWat20A/1e-9); hold on
plot(pulseWidthList, posNegWidthListWat30A/1e-9)
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Pulse Width [ns]')

legend(['20A'], ['30A']);

figure;
plot(pulseWidthList, negWidthListWat20A/1e-9); hold on
plot(pulseWidthList, negWidthListWat30A/1e-9)
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Pulse Width [ns]')
legend(['20A'], ['30A']);

figure;
plot(pulseWidthList, maxVal20A/1e-3); hold on
plot(pulseWidthList, maxVal30A/1e-3);
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Pulse Amplitude [mV]')
legend(['20A'], ['30A']);

%% Save
% save(['.\Procesado\Processed_Data_OSRAMDriverComercial_25MHz_20dB'], ...
%         'pulseWidthList', 'maxVal20A', 'maxVal30A', 'negWidthListWat30A', ...
%         'negWidthListWat20A', 'posNegWidthListWat30A', 'posNegWidthListWat20A',...
%         'posWidthListWat20A', 'posWidthListWat30A', 'bwList20A', 'bwList30A')
