function [f, S, bw] = spec(signal, fs, range)
    N_fft = 2^20;
    f = linspace(0,fs,N_fft);
    S = fft(signal,N_fft); 

    Sdb = 20*log10(abs(S)./max(abs(S)));
    [~, id] = min(abs(f - range));
    Sdb = Sdb(1:id);
    [~, idxmax] = max(Sdb);
    idx30db = 1;idx40db = 1; idx6db1 = 1;idx6db2=1;idx10db = 1;idx20db = 1;
    for k = idxmax:length(Sdb)
        if Sdb(k) < -6
            idx6db2 = k;
        break;
        end
    end
    for k = idxmax:-1:1
        if Sdb(k) < -6
            idx6db1 = k;
        break;
        end
    end
    for k = idx6db2:length(Sdb)
        if Sdb(k) < -10
            idx10db = k;
        break;
        end
    end
    for k = idx10db:length(Sdb)
        if Sdb(k) < -20
            idx20db = k;
        break;
        end
    end
    
    idx10db2 = 1;
    for k = idxmax:-1:1
        if Sdb(k) < -10
            idx10db2 = k;
        break;
        end
    end
    for k = idx20db:length(Sdb)
        if Sdb(k) < -30
            idx30db = k;
        break;
        end
    end
    
    idx40db = 1;
    for k = idx30db:length(Sdb)
        if Sdb(k) < -40
            idx40db = k;
        break;
        end
    end

    bw = [idx6db1, idx6db2, idx10db, idx10db2,idx20db, idx30db, idx40db];
    
end
