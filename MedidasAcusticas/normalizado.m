function [ signalNorm, maxVal,  minVal] = normalizado( signal)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

 [maxVal] = max(signal); [minVal] = min(signal);

    signalNorm = zeros(size(signal));
    for k = 1:length(signal)
        if signal(k) < 0
            signalNorm(k) = signal(k)/abs(minVal);
        else
            signalNorm(k) = signal(k)/maxVal;
        end
    end

end

