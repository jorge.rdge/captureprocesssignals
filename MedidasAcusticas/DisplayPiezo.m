close all; 
clear all;

%% Import and Order Data
rutaCapturasAcoustic = '.\Procesado\';

dataListDir = ls(rutaCapturasAcoustic);
dataListTmp = dataListDir(3:end-4, :);

W30 = importdata([rutaCapturasAcoustic, dataListTmp(11, :)]);
CMUT = importdata([rutaCapturasAcoustic, dataListTmp(16, :)]);
PZT30 = importdata([rutaCapturasAcoustic, dataListTmp(13, :)]);
% W30 = zeros(10,1400);
% 
% for k = 1:size(PZT30, 1)
%     W30(k, :) = [W30_tmp(k,:), zeros(1, size(PZT30, 2) - size(W30_tmp(k,:), 2))];
% end

dataListTmpTime = dataListDir(end-3:end, :);
time10tmp = importdata([rutaCapturasAcoustic, dataListTmpTime(2, :)]); % dt 5e-10
time100tmp = importdata([rutaCapturasAcoustic, dataListTmpTime(3, :)]);
time100 = time100tmp([7407:8218]);
timeCMUT = importdata([rutaCapturasAcoustic, dataListTmpTime(1, :)]);

%% Display
% 
% pulseWidth = [1, 2, 5, 10];
% 
% time10n = [time100; time10; time25];
% 
% figure;
% for k = 1:3
%     plot(time10n(k, :), data_10_20tmp(k,:)); hold on
% end

dtCMUT = timeCMUT(2)-timeCMUT(1);
dt100 = time100(2) - time100(1); 
dt10 = time10tmp(2) - time10tmp(1);
dt = 4e-10;

t100 = time100-(time100(1) - time10tmp(1)); %[time100(1):dt10:time100(end)];
tCMUTtmp = timeCMUT-(timeCMUT(1) - time10tmp(1)); %[time10(1):dtCMUT:time10(end)];

tCMUT = [tCMUTtmp(1):dt:tCMUTtmp(end)+dt];

t10 = [time10tmp(1):dt:time10tmp(end)+dt];


for sn = [1,5,10]
    CMUTres = resample(CMUT(sn,:), 2, 1);
    PZTres = resample(PZT30(sn,:), 5, 4);

    data = [PZTres(1:812); W30(sn, :); CMUTres(1:812)];
    
    % 0.0145
    offset = 0;
    if sn ~= 1;offset = -200;end
    [dataAl,idx] = alignData(data, 0.5, 1, offset);
    figure;
    for k = 1:3
        plot(t100/1e-6, dataAl(k,:)); hold on
%         plot(t100/1e-6, normalizado(W30(sn, :))); hold on
%         plot(t10/1e-6, normalizado(PZTres)); hold on
%         plot(tCMUT/1e-6, normalizado(CMUTres))
    end
    xlabel('us'); ylabel('a.u.');
    title([num2str(sn*10, '%2.0f ns acoustic signal comparison')])
    legend({'PZT-10MHz', 'Wat-13-100MHz', 'CMUT'})
    
    savefig(['.\Figures\__AcousticComparison_30A_WATPZT10CMUT_NORM',num2str(sn*10, '%2.0fns')]);
end

vh20A = [50, 40, 35, 32.5, 30, 30, 30, 30, 30, 30];
vh30A = [60, 55, 47.5, 47.5, 47.5, 45, 45, 45, 45, 45];
dataWidthList = 10:10:100;
    

%% FFT
for sn = [1,5,10]
    CMUTres = resample(CMUT(sn,:), 2, 1);
    PZTres = resample(PZT30(sn,:), 5, 4);

    data = [PZTres(1:812); W30(sn, :); CMUTres(1:812)];
    
    % 0.0145
    offset = 0;
    if sn ~= 1;offset = -200;end
    [dataAl,idx] = alignData(data, 0.5, 1, offset);
    
    idxAc = 1:812;
%     for k = 1:3
%         plot(t100/1e-6, dataAl(k,:)); hold on
%     end
    Ts = 4e-10;
    Fs = 1/Ts; Fn = Fs/2; 
    pulseWidthList = 10:10:100;
    legendCell = cellstr(num2str(pulseWidthList', '%-d ns'));

    % bwIdxList30A = zeros(length(data), 7);
    % bwList30A = zeros(size(bwIdxList30A));

    range = 70e6;

    figure;
    for k = 1:3
        [fvec, S] = spec(dataAl(k,idxAc), Fs, range);
    %     figure;
        plot(fvec/1e6, 20*log10(abs(S)./max(abs(S)))); hold on;
    %     plot(fvec(bwIdxList30A(k, :))/1e6, [-6, -6, -10, -10,-20, -30, -40], 'r*')
    end
    axis([0, range/1e6, -70, 2]);
    title([num2str(sn*10, '%2.0f ns acoustic signal FFT comparison')])
    xlabel('MHz'); ylabel('dB')
    line(fvec, linspace(-6, -6, length(fvec)), 'Color', 'k');
    line(fvec, linspace(-20, -20, length(fvec)), 'Color', 'k');
    line(fvec, linspace(-40, -40, length(fvec)), 'Color', 'k');
    line(fvec, linspace(-60, -60, length(fvec)), 'Color', 'k');

    legend({'PZT-10MHz', 'Wat-13-100MHz', 'CMUT'})
    savefig(['.\Figures\__AcousticComparison_30A_WATPZT10CMUT_NORM_FFT',num2str(sn*10, '%2.0fns')]);
end
