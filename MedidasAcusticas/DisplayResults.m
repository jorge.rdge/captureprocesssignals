close all; clear all;
%%
rutaProcessedData = '.\Tables\';

dataListDir = ls(rutaProcessedData);
% 3:7 100MHz
dataListTmp = dataListDir(11:14, :);

% 100 MHZ
% InputWidth_ns	HighVoltageInput_V	CurrentPulseWidth_ns	NegativeLobeWidth_ns	PositiveLobeWidth_ns	MaxMinWidth_ns	PressureAmplitude_Pa	PressureAmplitude_mV
T20 = xlsread([rutaProcessedData, dataListTmp(1,:)]);
T30 = xlsread([rutaProcessedData, dataListTmp(3,:)]);

yCell = {'ns', 'ns', 'ns','mV'};
titleCell = {'the acoustic positive lobe width', 'the acoustic negative lobe width', ...
            'the acoustic positive and negative width', 'acoustic amplitude'};
for k = 1:4
    figure;
    plot(T30(:, 1), T30(:, k+2)); hold on
    plot(T20(:, 1), T20(:, k+2))

    xlabel('ns'); ylabel(yCell{k});
    title(['Relation between the input pulse width and ',...
            titleCell{k}]);
        legend({'30 A', '20 A'});
    savefig(['.\Figures\', num2str(k, 'AcousticDataComparison25MHZ_%d')])
    saveas(gcf, ['.\Figures\PNG\', num2str(k, 'AcousticDataComparison25MHZ_%d')], 'png');
end


% Positive lobe
% Negative Lobe
% Pos Neg width


%%
rutaProcessedData = '.\Procesado\';

dataListDir = ls(rutaProcessedData);
% 3:7 100MHz

dataListTmp = dataListDir([4,6,11], :);

dataWat = importdata([rutaProcessedData, dataListTmp(1,:)]);
dataPZT = importdata([rutaProcessedData, dataListTmp(2,:)]);
dataCMUT = importdata([rutaProcessedData, dataListTmp(3,:)]);

figure
plot(dataCMUT.pulseWidthList, dataList.posWidthListWat30A/1e-9); hold on
plot(dataCMUT.pulseWidthList, dataList.negWidthListWat30A/1e-9);
plot(dataCMUT.pulseWidthList, dataList.posNegWidthListWat30A/1e-9);
title('Acoustic pulse widths comparison for 30A input');
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Pulse Width [ns]')
legend({'Positive Lobe', 'Negative Lobe', 'Distance between max and min '})

figure
plot(dataCMUT.pulseWidthList, dataList.posWidthListWat20A/1e-9); hold on
plot(dataCMUT.pulseWidthList, dataList.negWidthListWat20A/1e-9);
plot(dataCMUT.pulseWidthList, dataList.posNegWidthListWat20A/1e-9);
title('Acoustic pulse widths comparison for 20A input');
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Pulse Width [ns]')
legend({'Positive Lobe', 'Negative Lobe', 'Distance between max and min '})