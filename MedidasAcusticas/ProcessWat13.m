close all; 
clear all;

%% Import and Order Data
rutaCapturasAcoustic = '.\dataWat13\data20A\';

dataListDir = ls(rutaCapturasAcoustic);
dataListTmp = dataListDir(3:end-1, :);
dataList = repmat(' ', [size(dataListTmp)]);

dataWidth = zeros(size(dataListTmp, 1), 1);

for k = 1:size(dataListTmp,1)
    nameData = regexp(dataListTmp(k,:),'\d*','Match');
    idx = str2double(nameData{1})/10;
    dataWidth(idx) = idx*10;
    dataList(idx,:) =  dataListTmp(k,:);
end

dataExamp = load([rutaCapturasAcoustic, dataList(1,:)]);
data = repmat(dataExamp, size(dataList, 1), 1 );

for k = 1:length(data)
    data(k) = importdata([rutaCapturasAcoustic, dataList(k,:)]);
end
% clear dataExamp

%% Process current pulses

dataNorm = data;
for k = 1:length(dataWidth)
    tmp_current = normalizado(-dataNorm(k).current.averaged.signal);
    dataNorm(k).current.averaged.signal = -tmp_current;
    
    tmp_acoustic = normalizado(data(k).wat13.averaged.signal);
    dataNorm(k).wat13.averaged.signal = tmp_acoustic;
 
    for c = 1:length(data(1).current.sampled)
        tmp_current = normalizado(-data(k).current.sampled(c).signal);
        dataNorm(k).current.sampled(c).signal = tmp_current;
        tmp_acoustic = normalizado(data(k).wat13.sampled(c).signal);
        dataNorm(k).wat13.sampled(c).signal = tmp_acoustic;
    end 
end

% legendCell = cellstr(num2str([data_optic.vh]', 'N=%-d'));
time_current = data(1).current.averaged.t0 + data(1).current.averaged.dt*...
                (0:length(data(1).current.averaged.signal) - 1);
            
idxListCurrent = {[1300:1342], [1300:1365], [1300:1397], [1300:1417], [1300:1442], ...
                  [1300:1468], [1300:1492], [1300:1517], [1300:1542], [1300:1567]};
for k = 1:length(data)
    anchuras_current(k)=ancho_pulso(time_current(idxListCurrent{k}),...
        -dataNorm(k).current.averaged.signal(idxListCurrent{k}),0.5);
end
anchuras_current = anchuras_current/1e-9;


legendCell = cellstr(num2str(anchuras_current', '%0.2f ns'));
figure;
for k = 1:length(data)
    hold on;
    plot(time_current/1e-9, data(k).current.averaged.signal);
end
ylabel('V'); xlabel('ns'); legend(legendCell);

%% Process acoustic pulses Wat 13

time_acoustic = data(1).wat13.averaged.t0 + data(1).wat13.averaged.dt*...
                (0:length(data(1).wat13.averaged.signal) - 1);
idxListAcoustic = {[7407:7952], [7407:7958], [7407:7968], [7407:7977], [7407:7991]...
                  [7407:8012], [7407:8033], [7407:8085], [7407:8125], [7407:8218]};

idxListAcoustic = {[7407:8218], [7407:8218], [7407:8218], [7407:8218], [7407:8218],...
                    [7407:8218], [7407:8218], [7407:8218], [7407:8218], [7407:8218]};
                

plotOpt = 1;
posWidthListWat = zeros(length(dataWidth), 1);
negWidthListWat = zeros(length(dataWidth), 1);
posNegWidthListWat = zeros(length(dataWidth), 1);
maxVal = zeros(length(dataWidth), 1);

for k = 1:length(data)
[posWidthListWat(k), negWidthListWat(k), posNegWidthListWat(k), maxVal(k), rise30A(k)] = ...
    processPulse(time_acoustic, ...
                 data(k).wat13.averaged.signal, ...
                 idxListAcoustic{k}, 1, dataWidth(k));
end
plot([10:10:100], rise30A'/1e-9);hold on;plot([10:10:100], ones(1,10)*mean(rise30A)/1e-9)

%% Plot PA
zi = 0.15e6; % g/(cm2*s) Waters acoustic impedance
zc = 1.26e6; % g/(cm2*s) Wat 13 Conductor acoustic impedance
V2PA = 1e3*(1/(17*100*1e-5));
pC2pI = ((zc + zi)/(2*zc));

factor = V2PA*pC2pI;

legendCell = num2str(dataWidth, '%0.2f ns');
figure;
for k = 1:10
    hold on;
    plot(time_acoustic(idxListAcoustic{k})/1e-6, factor*data(k).wat13.averaged.signal(idxListAcoustic{k}));
    plot(time_acoustic/1e-6, factor*data(k).wat13.averaged.signal);

end
figure;
plot(time_acoustic/1e-6, factor*data(end).wat13.averaged.signal);

xlabel('us');ylabel('Pa');legend(legendCell)

%% FFT Acoustic WAT13
t_aco = time_acoustic(idxListAcoustic{end});

Ts = data(1).wat13.averaged.dt;
Fs = 1/Ts; Fn = Fs/2; 

legendCell = cellstr(num2str(dataWidth, '%-d ns'));
% for k = 1:size(data,1)
%     figure

%     subplot(2, 1,1)
%     plot(time_acoustic(idxListAcoustic{k}), ...
%             data(k).wat13.averaged.signal(idxListAcoustic{k})); 
%     hold on;
%     title(num2str(dataWidth(k)', '%-d ns'))
% 
%     subplot(2,1,2)
%     plotFreq(data(k).wat13.averaged.signal(idxListAcoustic{k}), Fs, 1e6, 70e6); hold on
% end

figure;
for k = 1:size(data,1)
   fvec = plotFreq(data(k).wat13.averaged.signal(idxListAcoustic{k}), Fs, 1e6, 70e6); hold on

end
title('Wat 13')
line(fvec, linspace(-20, -20, length(fvec)), 'Color', 'k');
line(fvec, linspace(-40, -40, length(fvec)), 'Color', 'k');
line(fvec, linspace(-60, -60, length(fvec)), 'Color', 'k');

xlabel('MHz'); ylabel('dB')
legend(legendCell);

%% Display Data

figure;
plot(dataWidth, anchuras_current)
xlabel('Input pulse Width [ns]');
ylabel('Current Pulse Width [ns]')

figure;
plot(dataWidth, posWidthListWat/1e-9)
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Positive Width [ns]')

figure;
plot(dataWidth, posNegWidthListWat/1e-9)
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Negative-Positive Width [ns]')

figure;
plot(dataWidth, negWidthListWat/1e-9)
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Negative Width [ns]')

figure;
plot(dataWidth, maxVal/1e-3)
xlabel('Input pulse Width [ns]');
ylabel('Amplitude [mV]')

figure;
plot(dataWidth, factor*maxVal)
xlabel('Input pulse Width [ns]');
ylabel('Amplitude [Pa]')