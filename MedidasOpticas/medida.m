
clear all
close all

hOsc=ds2000;

nSaves = 16;
autoScale = 0;

% hFig = figure;
% hAxes(1) = subplot(1,1,1);

%hOsc.saveMode = 'fullframe';
%hOsc.plotMode = 'fullframe';

hOsc.saveMode = 'preview';
hOsc.plotMode = 'preview';

hOsc.stopAfterSave = 1;
vhList = 0:2.5:60;
inWidth = 100;
%%
for k = 1:length(vhList)
    hOsc.acquire.mode = 'Sampled';
    % hOsc.trigger.sweep = 'normal';

    hOsc.channel(1).acquire = 1;
    hOsc.channel(1).line = line('color', 'y');
    hOsc.channel(1).save = nSaves;
    hOsc.channel(1).autoScale = autoScale;
    hOsc.channel(1).display=1;

    hOsc.channel(2).acquire = 1;
    hOsc.channel(2).line = line('color', 'b');
    hOsc.channel(2).save = nSaves;
    hOsc.channel(2).autoScale = autoScale;
    hOsc.channel(2).display=1;


    hOsc.start;
    wait(hOsc);
    data(k).currentPulse = hOsc.channel(1).signal;
    data(k).opticalPulse = hOsc.channel(2).signal;

    hOsc.channel(1).save = 1;
    hOsc.channel(2).save = 1;
    hOsc.acquire.mode = 'Average';
    hOsc.acquire.nAverages = 16;

    hOsc.start;
    wait(hOsc);

    data(k).currentPulseAverage = hOsc.channel(1).signal;
    data(k).opticalPulseAverage = hOsc.channel(2).signal;
    
    pow = input(['Measured Power for ', num2str(vhList(k)),' V in mW: ']);
    data(k).power = pow;
    data(k).vh = vhList(k);
    
    figure
    plot([data(k).currentPulse.signal])
     figure
    plot(data(k).currentPulseAverage.signal)
    figure
    plot([data(k).opticalPulse.signal])
     figure
    plot(data(k).opticalPulseAverage.signal)
    
    cont = input(['Continue with measure ' num2str(vhList(k)+2.5, '%2d V?')]);
    if isempty(cont); cont = 1;end;
    if cont; continue; close all; else break;end;
end


%%
 currDate = clock;
 dateStr = [];
 for k = 1:length(currDate)
     dateStr = [dateStr '-' num2str(round(currDate(k)))];
 end
 
save(['OSRAMDriverComercial_measurement' dateStr, num2str(inWidth, '_%d_ns'), '.mat'],'data');

hOsc.acquire.mode = 'Sampled';
hOsc.runAcquire;

delete(hOsc);
%%
figure;
for k = 8:length(data)
    sig = data(k).opticalPulseAverage.signal/max(data(k).opticalPulseAverage.signal);
%     fs = 2/data(k).opticalPulseAverage.dt;
%     sigtmp = lowpass(sig, 100e6, fs); %smooth(sig, 0.05, 'loess');
%     if k == 25; idx = 0;end
    sigtmp = smooth(sig, 10);
    plot(sigtmp); hold on;
end
%%
[val, idx] = min(data(k).opticalPulseAverage.signal);
figure;plot(circshift(data(k).opticalPulseAverage.signal, idx));
hold on; plot(data(k).opticalPulseAverage.signal)
