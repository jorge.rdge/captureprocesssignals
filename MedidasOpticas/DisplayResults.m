close all; clear all;

rutaProcessedData = '.\Procesado\';

dataListDir = ls(rutaProcessedData);
dataListTmp = dataListDir(3:end-1, :);

dataExamp = load([rutaProcessedData,dataListTmp(end, :)]);
dataList = repmat(dataExamp, [10,1]);

for k = 1:length(dataList)
    dataList(k) = importdata([rutaProcessedData, dataListTmp(k,:)]);
end

%%


for k = 1:length(dataList)
    maxPP(k) = dataList(k).meanPower(end);
%     mean([dataList(k).PpList(4:end)]);
end

maxPP = sort(maxPP)'
T = table([10:10:100]', maxPP)
writetable(T, '.\OpticalPulseComparisonPowerPeak.xlsx')

%%
plotData(dataList);
% 
% for k = 1:length(dataList)
%     vh30A(k) = dataList(k).vhList(end);
%     curr30A(k) = dataList(k).maxCurr(end);
%     
%     vh20A(k) = dataList(k).vhList(end-9);
%     curr20A(k) = dataList(k).maxCurr(end-9);
% end
