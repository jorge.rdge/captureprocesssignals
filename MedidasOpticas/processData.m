for ink = [4,5,7]

    % Import and Order Data
    rutaCapturasAcoustic = '.\Medidas\';

    dataListDir = ls(rutaCapturasAcoustic);
    dataListTmp = dataListDir(3:end-2, :);

    % dataExamp = importdata([rutaCapturasAcoustic, dataListTmp(1,:)]);

    % dataList = cell(10,1);repmat(dataExamp, [10,1]);
    % dataWidth = zeros(size(dataListTmp, 1), 1);
    % 
    for k = 1:size(dataListTmp,1)
        nameData = regexp(dataListTmp(k,end-9:end),'\d*','Match');
        idx = str2double(nameData{1})/10;
        dataWidth(k) = idx*10;
    end

    % dataExamp = load([rutaCapturasAcoustic, dataList(1,:)]);
    % data = repmat(dataExamp, size(dataList, 1), 1 );
    % 
    % for k = 1:length(data)
    %     data(k) = importdata([rutaCapturasAcoustic, dataList(k,:)]);
    % end
    % clear dataExamp
%     dataWidth
%     ink = 7;
    data = importdata([rutaCapturasAcoustic, dataListTmp(ink,:)]);
    vh = [data.vh]';
    legendCell = cellstr(num2str(vh(1:end-1), '%0.1f V'));
    time_optical = data(1).opticalPulseAverage.t0 + data(1).opticalPulseAverage.dt*...
                    (0:length(data(1).opticalPulseAverage.signal) - 1);         
    figure;
    for k = 1:length(data)-1
        plot(time_optical/1e-9, data(k).opticalPulseAverage.signal(:)); hold on;
    end
    title(['Optical Pulses for ', num2str(ink*10, '%d ns input')])
    ylabel('mV'); xlabel('ns'); legend(legendCell);
    xlim([50, 200])
%     saveas(gcf, ['.\Figures\PNG\OpticalPulse', num2str(ink*10, '%d_ns')], 'png')
    
    close all; clear all;
end

%% Process current pulses
time_current = data(1).currentPulseAverage.t0 + data(1).currentPulseAverage.dt*...
                (0:length(data(1).currentPulseAverage.signal) - 1);
figure;
for k = 1:length(data)
    plot(data(k).currentPulseAverage.signal(:)); hold on;
end

offsetCurr = [1, 3, 1, 1, 1, 1, 1, 1, 1, 1];
idxCurrent = {251:335, 240:385, 240:445, 230: 534, 240: 486, 230:586, ...
                225:639, 225:691, 225:742, 230:791};
              
for k = offsetCurr(ink):length(data)
    [normTmp, maxCurr(k)] = normalizado(-data(k).currentPulseAverage.signal(idxCurrent{ink}));
    anchuras_current(k)=ancho_pulso(time_current(idxCurrent{ink}), normTmp,0.5);
end

vh = [data.vh]';
legendCell = cellstr(num2str(vh(1:end-1), '%0.1f V'));
figure;
for k = 1:length(data)-11
    hold on;
    plot(time_current/1e-9, smooth(data(k).currentPulseAverage.signal, 15));
end
title(['Current Pulses for ', num2str(ink*10, '%d ns input')])
ylabel('V'); xlabel('ns'); legend(legendCell);
xlim([50, 200])
% saveas(gcf, ['.\Figures\PNG\CurrentPulse', num2str(ink*10, '%d_ns_ZOOM_2')], 'png')
%% Process optical pulses Wat 13

time_optical = data(1).opticalPulseAverage.t0 + data(1).opticalPulseAverage.dt*...
                (0:length(data(1).opticalPulseAverage.signal) - 1);              
% 
% figure;
% for k = 1:length(data)
%     plot(data(k).opticalPulseAverage.signal(:)); hold on;
% end

idxOptical = {240:480, 240:536, 240:592, 240:687, 240:630, 240:738,...
                240:786, 240:839, 240:876, 240:936};
offsetOpt = [1, 1, 1, 1, 1, 3, 3, 2, 3, 1];

plotOpt = 0;
opticalPulseWidth = zeros(length(data), 1);

opticalPulseAmplitude = zeros(length(data), 1);
vhList = 0:2.5:data(end).vh;

pulseOpt = 'Optical';
for k = offsetOpt(ink):length(data)
[opticalPulseWidth(k), ~, ~, opticalPulseAmplitude(k)] = ...
    processPulse(time_optical, ...
                 data(k).opticalPulseAverage.signal, ...
                 idxOptical{ink}, 1, vhList(k), pulseOpt);
end

for k = 1:length(data)
    normTmp = normalizado(data(k).opticalPulseAverage.signal(idxOptical{ink}));
    areaList(k)=trapz(time_optical(idxOptical{ink}),normTmp)/1e-9;
    PpList(k)= data(k).power/(1e-3*areaList(k));
end

vh = [data.vh]';
legendCell = cellstr(num2str(vh(1:end-1), '%0.1f V'));
time_optical = data(1).opticalPulseAverage.t0 + data(1).opticalPulseAverage.dt*...
                (0:length(data(1).opticalPulseAverage.signal) - 1);         
figure;
for k = 1:length(data)-1
    plot(time_optical/1e-9, data(k).opticalPulseAverage.signal(:)); hold on;
end
title(['Optical Pulses for ', num2str(ink*10, '%d ns input')])
ylabel('mV'); xlabel('ns'); legend(legendCell);
xlim([50, 250])
saveas(gcf, ['.\Figures\PNG\OpticalPulse', num2str(ink*10, '%d_ns')], 'png')

%% FFT Optical
t_aco = time_optical(idxOptical);

Ts = data(1).opticalPulseAverage.dt;
Fs = 1/Ts; Fn = Fs/2; 

legendCell = cellstr(num2str(vhList', '%-d ns'));
% for k = 1:size(data,1)
%     figure

%     subplot(2, 1,1)
%     plot(time_acoustic(idxListAcoustic{k}), ...
%             data(k).wat13.averaged.signal(idxListAcoustic{k})); 
%     hold on;
%     title(num2str(dataWidth(k)', '%-d ns'))
% 
%     subplot(2,1,2)
%     plotFreq(data(k).wat13.averaged.signal(idxListAcoustic{k}), Fs, 1e6, 70e6); hold on
% end

figure;
for k = 1:length(data)
   fvec = plotFreq(data(k).opticalPulseAverage.signal(idxOptical), Fs, 1e6, 70e6); hold on

end
title('Wat 13')
line(fvec, linspace(-20, -20, length(fvec)), 'Color', 'k');
line(fvec, linspace(-40, -40, length(fvec)), 'Color', 'k');
line(fvec, linspace(-60, -60, length(fvec)), 'Color', 'k');

xlabel('MHz'); ylabel('dB')
legend(legendCell);

%% Display Data

figure;
plot(vhList, anchuras_current/1e-9)
xlabel('High Voltage Input [V]');
ylabel('Current Pulse Width [ns]')
title(num2str(dataWidth(ink), '%d ns'))

figure;
plot(vhList, opticalPulseWidth/1e-9)
xlabel('High Voltage Input [V]');
ylabel('Optical Pulse Width [ns]')
title(num2str(dataWidth(ink), '%d ns'))

figure;
plot(vhList, maxCurr*10)
xlabel('High Voltage Input [V]');
ylabel('Current Pulse mplitude [A]')

figure;
plot(vhList, opticalPulseAmplitude/1e-3)
xlabel('High Voltage Input [V]');
ylabel('Optical Pulse Amplitude [mV]')

figure;
plot(vhList, PpList)
xlabel('High Voltage Input [V]');
ylabel('Peak Power [W]')

figure;
meanPower = [data.power];
plot(vhList, meanPower)
xlabel('High Voltage Input [V]');
ylabel('Energy per pulse [uJ]')

%% Save
% dataWidth = dataWidth(ink);
% save(['.\Procesado\Processed_Data_OSRAMDriverComercial_', ...
%         num2str(dataWidth, '%d_ns.mat')], ...
%         'vhList', 'meanPower', 'PpList', 'opticalPulseAmplitude', ...
%         'maxCurr', 'anchuras_current', 'opticalPulseWidth', 'dataWidth')
