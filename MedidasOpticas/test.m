
close all;

h = rtb2000;        

h.saveMode = 'preview';
h.plotMode = 'preview';
h.stopAfterSave = 0;

colors = 'brcm';
idx = [1];
for k = 1:length(idx);
%k = 1;
    h.channel(idx(k)).acquire = 1;
    h.channel(idx(k)).line = line('color',colors(idx(k)));
    h.channel(idx(k)).autoScale = 0;
    h.channel(idx(k)).save = 20;
    h.channel(idx(k)).autoScale = 1;
end

idx =  find(idx ~= 1:4);
for k = 1:length(idx);
%k = 1;
    h.channel(idx(k)).acquire = 0;
    h.channel(idx(k)).autoScale = 0;
    h.channel(idx(k)).save = 10;    
end

%%
% k = 3;
% h.channel(k).acquire = 1;
% h.channel(k).line = line('color','r');
% h.channel(k).autoScale = 0;
% h.channel(k).save = 20;