close all; clear all;


rutaCapturasAcoustic = '.\Figures\';

dataListDir = ls(rutaCapturasAcoustic);
dataListTmp = dataListDir(3:7, :);

rutaCapturasAcousticSave = '.\Figures\PNG\';
for k = 1:size(dataListTmp, 1)
    h = openfig([rutaCapturasAcoustic, dataListTmp(k, :)], 'new');
    saveas(h, [rutaCapturasAcousticSave, dataListTmp(k, :)], 'png');
%     xlim([200, 1000])
%     saveas(h, [rutaCapturasAcousticSave, dataListTmp(k,:),'_ZOOM'], 'png');
    close all;
end