function [f, S] = plot_freq(signal, fs, sc, range)
    N_fft = 2^20;
    f = linspace(0,fs,N_fft);
    S = fft(signal,N_fft); 
    plot(f/sc,20*log10(abs(S)./max(abs(S)))); hold on;
  axis([0, range/sc, -80, 2]);
end
