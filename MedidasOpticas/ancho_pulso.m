function [ ancho, idx1, idx2] = ancho_pulso(tiempo,pulso,criterio )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[maximo,posMax]=max(pulso);

for k=posMax:-1:0
    
    if(pulso(k)<criterio)
    posicion1=tiempo(k);
    idx1 = k;
    break;
    end
end

for k=posMax:1:length(pulso)
    
    if(pulso(k)<criterio)
    posicion2=tiempo(k);
    idx2 = k;
    break;
    end
end

ancho= posicion2-posicion1;
end
