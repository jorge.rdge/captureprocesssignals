% Initialize medium's grid
medium.sound_speed = material_free_space.sound_speed*ones(Nx, Ny, Nz);
% medium.sound_speed(pos_package{1}, pos_package{2},pos_package{3}) = material_package.sound_speed;
% medium.sound_speed(pos_separation{1}, pos_separation{2},pos_separation{3}) = material_package.sound_speed;
% medium.sound_speed(pos_emisor{1}, pos_emisor{2},pos_separation{3}) = material_emisor.sound_speed;
medium.sound_speed(pos_cnt{1}, pos_cnt{2},pos_cnt{3}) = material_cnt.sound_speed;
medium.sound_speed(pos_pdms{1}, pos_pdms{2},pos_pdms{3}) = material_pdms.sound_speed;

medium.sound_speed(pos_sensor_conducting{1}, pos_sensor_conducting{2},pos_sensor_conducting{3}) = material_sensor_conducting.sound_speed;
medium.sound_speed(pos_sensor_sensing{1}, pos_sensor_sensing{2},pos_sensor_sensing{3}) = material_sensor_conducting.sound_speed;

% Initialize medium's grid
medium.density = material_free_space.density*ones(Nx, Ny, Nz);
% medium.density(pos_package{1}, pos_package{2},pos_package{3}) = material_package.density;
% medium.density(pos_separation{1}, pos_separation{2},pos_separation{3}) = material_package.density;
% medium.density(pos_emisor{1}, pos_emisor{2},pos_separation{3}) = material_emisor.density;
medium.density(pos_cnt{1}, pos_cnt{2},pos_cnt{3}) = material_cnt.density;
medium.density(pos_pdms{1}, pos_pdms{2},pos_pdms{3}) = material_pdms.density;

medium.density(pos_sensor_conducting{1}, pos_sensor_conducting{2},pos_sensor_conducting{3}) = material_sensor_conducting.density;
medium.density(pos_sensor_sensing{1}, pos_sensor_sensing{2},pos_sensor_sensing{3}) = material_sensor_conducting.density;


% Initialize medium's grid
% medium.alpha_coeff = material_free_space.alpha*ones(Nx, Ny);
% medium.alpha_coeff(pos_back_x, pos_back_y) = material_back.alpha;
% medium.alpha_coeff(pos_abs_x, pos_abs_y) = material_abs.alpha;
% medium.alpha_mode('no_dispersion');
% medium.alpha_power = 1.01;
% medium.alpha_coeff = 1;
% c_ref = min(medium.sound_speed(:));
% mediuem.sound_speed_ref = c_ref;