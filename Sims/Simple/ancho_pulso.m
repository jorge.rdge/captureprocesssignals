function [ ancho, idx1, idx2, risetime] = ancho_pulso(tiempo,pulso,criteria )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[maximo,posMax]=max(pulso);
criterio = maximo*criteria;
for k=posMax:-1:0
    
    if(pulso(k)<criterio)
    posicion1=tiempo(k);
    idx1 = k;
    break;
    end
end

for k=posMax:1:length(pulso)
    
    if(pulso(k)<criterio)
    posicion2=tiempo(k);
    idx2 = k;
    break;
    end
end

for k=posMax:-1:1
    
    if(pulso(k)<0.9)
    posicion90=tiempo(k);
    idx1 = k;
    break;
    end
end
for k=posMax:-1:1
    
    if(pulso(k)<0.1)
    posicion10=tiempo(k);
    idx1 = k;
    break;
    end
end

risetime = posicion90-posicion10;
ancho= posicion2-posicion1;
end
