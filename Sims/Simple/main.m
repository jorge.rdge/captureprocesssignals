clear all; 
% close all;

PML_SIZE = 10;
PML_ALPHA = 10;
CFL = 0.3;

% Define optional inputs
input_args = {'DataCast', 'single',...
             'PlotLayout', false, 'PlotPML', false, ...
			 'PMLSize', PML_SIZE, 'PMLAlpha', PML_ALPHA, ...
			 'RecordMovie', false, 'PlotScale', [-3 3], 'DataRecast', true, ...
             'CartInterp', 'nearest', 'DataCast', 'gpuArray-single'};
%% ========================================================================
% Set Simulation
% =========================================================================

pulse_width = 10e-9;
emisor_cnt_distance_list = [0, 50, 100, 200]*1e-6;%[0, 10, 20, 30, 40, 50, 100]*1e-6;
emisor_cnt_distance = 100e-6;
set_simulation;

emptycell = {[1:1],[1:1],[1:1]};
% Plot Experiment
plot_experiment( medium.sound_speed , pos_cnt, emptycell, emptycell, pos_pdms,...
                pos_presure, emptycell, pos_sensor_sensing, ...
                emptycell, Nx, Ny, Nz, kgrid.x_vec, kgrid.y_vec, kgrid.z_vec )
% str = input('Proceed with the simulation?', 's');
% if strcmp(str, 'N') , return, end;

%% SIMULATION
sensor_data_collection = zeros(length(emisor_cnt_distance_list), kgrid.Nt);

% for c = 1:length(emisor_cnt_distance_list);
%     emisor_cnt_distance = emisor_cnt_distance_list(c);
%     set_simulation;
    sensor_data2 = kspaceFirstOrder3DG(kgrid, medium, source, sensor, input_args{:});
%     sensor_data_collection(c, :) = mean(sensor_data.p);
% end


%% =========================================================================
% VISUALISATION
% =========================================================================

% extract a suitable axis scaling factor
[~, scale, prefix] = scaleSI(kgrid.t_array);
for k = 1:size(sensor_data2.p, 1)
    [maxVal2(k), maxIdx(k)] = max( sensor_data2.p(k, :));
end
[maxTot2, maxIdx] = max(maxVal2);
figure;
idx = [1:size(sensor_data2.p, 1)-1];
c = 1;
for k = idx(1:14:(length(idx)))
    h = subplot(35, 1, c);
%     plot(t1*scale, sensor_data.p(k, :)); hold on;
    plot(kgrid.t_array*scale, sensor_data2.p(k, :), 'b'); hold on;
    ylabel(num2str([c,maxVal2(k)], 'Sensor: %d - %2.3f a.u.'), 'Rotation',0)
%     ylim([-maxTot2-maxTot2*0.5, maxTot2+maxTot2*0.5])
    ylh = get(gca,'ylabel');
    gyl = get(ylh);                                                         % Object Information
    ylp = get(ylh, 'Position');
    set(ylh, 'Rotation',0, 'Position',ylp, 'VerticalAlignment','middle', 'HorizontalAlignment','center')
    set(h,'ytick',[])
    if k ~= 49
        set(h,'xtick',[])
    else
        xlabel('us')
    end
    if k == 1
        title('Acoustic sginal distribution - Simulation')
    end
    c = c+1;
end
%%
figure;
idxZoom = 100:180;
plot(kgrid.t_array(idxZoom)*scale, sensor_data2.p(maxIdx,idxZoom)); hold on;
sigMea = mean(sensor_data2.p);
plot(kgrid.t_array(idxZoom)*scale, sigMea(idxZoom));
xlabel('us'); ylabel('a.u.'); title('Simulation')
legend({'Max signal','Mean signal'});

t1 = ancho_pulso(kgrid.t_array(idxZoom), sensor_data2.p(maxIdx,idxZoom),0.5)/1e-9
t2 = ancho_pulso(kgrid.t_array(idxZoom), sigMea(idxZoom),0.5)/1e-9



