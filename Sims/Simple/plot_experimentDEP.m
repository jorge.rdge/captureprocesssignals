[x_sc, scale, prefix] = scaleSI(max([x, y]));
figure;
subplot(2, 1, 1)
imagesc(kgrid.y_vec(pos_back_y)*scale, kgrid.x_vec(pos_back_x)*scale, double(medium.sound_speed(pos_back_x,pos_back_y)));
hold on;
imagesc(kgrid.y_vec(pos_abs_y)*scale, kgrid.x_vec(pos_abs_x)*scale, double(medium.sound_speed(pos_abs_x,pos_abs_y)));

set(gca, 'XLim', scale*[-y/2 y/2], 'YLim', scale*[-x/2 x/2]);
title('Sound of speed distribution of the back panel, absorbent and fibre');
xlabel(['distance [' prefix 'm]']);

subplot(2, 1, 2)
imagesc(kgrid.y_vec(pos_source_y)*scale, kgrid.x_vec(pos_source_x)*scale, double(medium.sound_speed(pos_source_x,pos_source_y)));
hold on;
imagesc(kgrid.y_vec(pos_sensor_y)*scale, kgrid.x_vec(pos_sensor_x)*scale, double(medium.sound_speed(pos_sensor_x,pos_sensor_y)));
set(gca, 'XLim', scale*[-y/2 y/2], 'YLim', scale*[-x/2 x/2]);
title('Sources Sound of speed distribution and sensor mask');
xlabel(['distance [' prefix 'm]']);