%% =========================================================================
% SIMULATION PARAMETERS
% =========================================================================
materials;
material_emisor = silica;
material_package = polyethylene;
material_cnt = cnt;
material_pdms = pdms;
material_free_space = water;

material_sensor_conducting = water;
material_sensor_sensing = water;

set_grid;

thick_cnt = 50; % um
thick_pdms = 10; % um

% Apertura Emisor
height_presure = 10; % um
width_presure = 200; % um
% height_presure = emisor_cnt_distance*tan((9*pi/180)/2)*2;
% width_presure = emisor_cnt_distance*tan((25*pi/180)/2)*2;
% 
% height_presure_adj = ceil(height_presure/dy);
% width_presure_adj = ceil(height_presure/dy);

dim_emisor = [400, 50, 400]*1e-6; % x, y, z
dim_package = [700, 700, 700]*1e-6; % x, y, z
dim_cnt = [700, thick_cnt, 700]*1e-6; % x, y, z
dim_pdms = [500, thick_pdms, 500]*1e-6; % x, y, z

dim_presure = [200, 10, 200]*1e-6; % x, y, z

dim_sensor_conducting = [1, 1, 1]*1e-6;
dim_sensor_sensing = [1200, 10, 10]*1e-6;

set_experiment;
set_media;

distance_from_sensor = (double(pos_sensor_conducting{2}(1)-pos_pdms{2}(end))*dy)/1e-6; % um


%% =========================================================================
% TIME SETTINGS
% =========================================================================

% [kgrid.t_array, dt] = makeTime(kgrid, medium.sound_speed, 0.3, t_end);
t_end = kgrid.y_size/min(medium.sound_speed(:));
kgrid.makeTime(medium.sound_speed, CFL, t_end);

%% =========================================================================
% SOURCE SETTINGS
% =========================================================================

% define source element
source.p_mask = zeros(Nx, Ny, Nz);
source.p_mask(pos_presure{1}, pos_presure{2}, pos_presure{3}) = 1;

% define a time varying sinusoidal source
source_mag = 10;     % [Pa]

num_sources = sum(source.p_mask(:));
t0 = 0.1e-6; Nt0 = int16(t0/kgrid.dt);
tend = t0 + pulse_width; Ntend = int16(tend/kgrid.dt);
pulse = zeros(1, kgrid.Nt);
pulse(Nt0:Ntend) = source_mag;

source.p = zeros(num_sources,kgrid.Nt);
for k = 1:num_sources
    source.p(k,:) = pulse;
end

% source.p = define_source_2D('None', pulse_width, kgrid.dt, kgrid.t_array, source, pos_presure{1},pos_presure{2});

% filter the source to remove high frequencies not supported by the grid
% source.p = filterTimeSeries(kgrid, medium, source.p);

%% ========================================================================
% SENSOR SETTINGS
% =========================================================================

% define a series of Cartesian points to collect the data
sensor.record = {'p', 'p_final'};
sensor.mask = zeros(Nx, Ny, Nz);
sensor.mask(pos_sensor_sensing{1}, pos_sensor_sensing{2}, pos_sensor_sensing{3}) = 1;  % [mm]

% sensor.frequency_response = [50e6, 100];
% define the field parameters to record
sensor.record = {'p', 'p_final'};
%% Check stability
dt_stability_limit = checkStability(kgrid, medium);
if kgrid.dt > dt_stability_limit
    t_end = kgrid.y_size/min(medium.sound_speed(:));
    kgrid.setTime(ceil(t_end/dt_stability_limit), dt_stability_limit)
end
