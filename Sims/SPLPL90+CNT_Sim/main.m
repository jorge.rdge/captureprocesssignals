clear all; 
% close all;

PML_SIZE = 10;
PML_ALPHA = 10;
CFL = 0.3;

% Define optional inputs
input_args = {'DataCast', 'single',...
             'PlotLayout', false, 'PlotPML', false, ...
			 'PMLSize', PML_SIZE, 'PMLAlpha', PML_ALPHA, ...
			 'RecordMovie', false, 'PlotScale', [-3 3], 'DataRecast', true, ...
             'CartInterp', 'nearest', 'DataCast', 'gpuArray-single'};
%% ========================================================================
% Set Simulation
% =========================================================================

pulse_width = 50e-9;
emisor_cnt_distance_list = [0, 50, 100, 200]*1e-6;%[0, 10, 20, 30, 40, 50, 100]*1e-6;
emisor_cnt_distance = 100e-6;
set_simulation;

% Plot Experiment
plot_experiment( medium.sound_speed , pos_cnt, pos_emisor, pos_package, pos_pdms,...
                pos_presure, pos_sensor_conducting, pos_sensor_sensing, ...
                pos_separation, Nx, Ny, Nz, kgrid.x_vec, kgrid.y_vec, kgrid.z_vec )
str = input('Proceed with the simulation?', 's');
if strcmp(str, 'N') , return, end;

%% SIMULATION
sensor_data_collection = zeros(length(emisor_cnt_distance_list), kgrid.Nt);

for c = 1:length(emisor_cnt_distance_list);
    emisor_cnt_distance = emisor_cnt_distance_list(c);
    set_simulation;
    sensor_data = kspaceFirstOrder3DG(kgrid, medium, source, sensor, input_args{:});
    sensor_data_collection(c, :) = mean(sensor_data.p);
end

figure;
plot(sensor_data_collection')

%% =========================================================================
% VISUALISATION
% =========================================================================

% extract a suitable axis scaling factor
[~, scale, prefix] = scaleSI(kgrid.t_array); 
figure;
plot(kgrid.t_array*scale, mean(sensor_data.p)); hold on;
%     plot(kgrid.t_array*scale, mean(sensor_data1.p)); hold on;

% plot(kgrid.t_array*scale, filterTimeSeries(kgrid,medium,mean(sensor_data.p)))
xlabel(['t [' prefix 's]']);
ylabel('A.u.');

        
%% SAVE
t = kgrid.t_array; pres = sensor_data.p;
save([num2str(emisor_cnt_distance/1e-6), '_ns_pulse.mat'],'pres', 'pulse_width', 'thick_cnt', ...
    'thick_pdms', 'emisor_cnt_distance', 't')

%% Load & Compare

dataList = ls('.\');
dataListdef = dataList([3,4,5,7,9,10,11], :);
for k = 1:length(emisor_cnt_distance_list)
    data(k) = importdata(dataListdef(k, :));
end
   figure; 
for k = 3:length(emisor_cnt_distance_list)
 plot(t, mean(data(k).pres));hold on;
end