function [] = voxelPlotM(pos_cnt, pos_emisor, pos_package, pos_pdms,...
                        pos_presure, pos_sensor_conducting, pos_sensor_sensing, ...
                        pos_separation, Nx, Ny, Nz)
    %VOXELPLOTM Summary of this function goes here
    %   pos --> 3x1 cell
    %   medium --> K-Wave medium

    alphaBack = 0.5;
    alphaFore = 1;

    % Plot Package
    start = [pos_package{1}(1),pos_package{2}(1),pos_package{3}(1)];
    endPoint = [pos_package{1}(end),pos_package{2}(end),pos_package{3}(end)];
    voxel(start, endPoint-start, 'b',alphaBack);hold on;

    % Plot Separation
    start = [pos_separation{1}(1),pos_separation{2}(1),pos_separation{3}(1)];
    endPoint = [pos_separation{1}(end),pos_separation{2}(end),pos_separation{3}(end)];
    voxel(start, endPoint-start, 'y',alphaBack);hold on;

    % Plot cnt
    start = [pos_cnt{1}(1),pos_cnt{2}(1),pos_cnt{3}(1)];
    endPoint = [pos_cnt{1}(end),pos_cnt{2}(end),pos_cnt{3}(end)];
    voxel(start, endPoint-start, 'r',alphaBack);hold on;

    % Plot Pdms
    start = [pos_pdms{1}(1),pos_pdms{2}(1),pos_pdms{3}(1)];
    endPoint = [pos_pdms{1}(end),pos_pdms{2}(end),pos_pdms{3}(end)];
    voxel(start, endPoint-start, 'b',alphaBack);hold on;

    % Plot Source
    start = [pos_presure{1}(1),pos_presure{2}(1),pos_presure{3}(1)];
    endPoint = [pos_presure{1}(end),pos_presure{2}(end),pos_presure{3}(end)];
    voxel(start, endPoint-start, 'g',alphaFore);hold on;

    % Plot Sensor
    start = [pos_sensor_conducting{1}(1),pos_sensor_conducting{2}(1),pos_sensor_conducting{3}(1)];
    endPoint = [pos_sensor_conducting{1}(end),pos_sensor_conducting{2}(end),pos_sensor_conducting{3}(end)];
    voxel(start, endPoint-start, 'b',alphaFore);hold on;

    start = [pos_sensor_sensing{1}(1),pos_sensor_sensing{2}(1),pos_sensor_sensing{3}(1)];
    endPoint = [pos_sensor_sensing{1}(end),pos_sensor_sensing{2}(end),pos_sensor_sensing{3}(end)];
    voxel(start, endPoint-start, 'y',alphaFore);hold on;

    % Plot Emisor
    start = [pos_emisor{1}(1),pos_emisor{2}(1),pos_emisor{3}(1)];
    endPoint = [pos_emisor{1}(end),pos_emisor{2}(end),pos_emisor{3}(end)];
    voxel(start, endPoint-start, 'y',alphaFore);hold off;

    axis([0, Nx, 0, Ny, 0, Nz]);
    xlabel('x'); ylabel('y'); zlabel('z');
    title('3D plot')

end

