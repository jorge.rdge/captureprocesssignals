clear all; close all;
materials;
material_emisor = glass_quartz;
set_grid;


dim_emisor = [400, 50, 400]*1e-6; % x, y, z
dim_package = [700, 700, 700]*1e-6; % x, y, z

Nx0 = Nx/2; Ny0 = dim_package(2)/dy; Nz0 = dim_package(3)/dz;

N_emisor_x = int16([Nx0 - dim_emisor(1)/(2*dx),...
                    Nx0 + dim_emisor(1)/(2*dx), ...
                    0]);                        % x1, x2, x3
N_emisor_y = int16([Ny0 - dim_emisor(2)/dy,... 
                    Ny0, ...
                    0]);                        % y1, y2, y3
N_emisor_z = N_emisor_x;                      % z1, z2, z3

pos_emisor = {N_emisor_x(1):N_emisor_x(2); ...
                N_emisor_y(1):N_emisor_y(2);...
                N_emisor_x(1):N_emisor_x(2);}; % x_vec, y_vec
            
            % Initialize medium's grid
medium.sound_speed = water.sound_speed*ones(Nx, Ny, Nz);
medium.sound_speed(pos_emisor{1}, pos_emisor{2},pos_emisor{2}) = material_emisor.sound_speed;

% Initialize medium's grid
medium.density = water.density*ones(Nx, Ny, Nz);
medium.density(pos_emisor{1}, pos_emisor{2},pos_emisor{2}) = material_emisor.density;

voxelPlot(medium.sound_speed)

