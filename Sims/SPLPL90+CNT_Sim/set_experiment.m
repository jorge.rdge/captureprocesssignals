%% Reference
Nx0 = Nx/2; Ny0 = dim_package(2)/dy; Nz0 = dim_package(3)/dz;

%% Emisor

N_package_x = int16([Nx0 - dim_package(1)/(2*dx),...
                    Nx0 + dim_package(1)/(2*dx), ...
                    0]);                        % x1, x2, x3
N_package_y = int16([1, Ny0, 0]);               % y1, y2, y3
N_package_z = int16([Nz0 - dim_package(3)/(2*dz),...
                    Nz0 + dim_package(3)/(2*dz), ...
                    0]);                        % z1, z2, z3

N_emisor_x = int16([Nx0 - dim_emisor(1)/(2*dx),...
                    Nx0 + dim_emisor(1)/(2*dx), ...
                    0]);                        % x1, x2, x3
N_emisor_y = int16([Ny0 - dim_emisor(2)/dy,... 
                    Ny0, ...
                    0]);                        % y1, y2, y3
N_emisor_z = int16([Nz0 - dim_emisor(3)/(2*dz),...
                    Nz0 + dim_emisor(3)/(2*dz), ...
                    0]);                    % z1, z2, z3

N_separation_x = N_package_x;                   % x1, x2, x3
N_separation_y = int16([Ny0, ...
                        Ny0 + emisor_cnt_distance/dy,...
                        0]);                    % y1, y2, y3
N_separation_z = N_package_z;


%% Deposición
N_cnt_x = N_package_x;                             % x1, x2, x3
N_cnt_y = int16([N_separation_y(2), ...
                 N_separation_y(2) + 1 + dim_cnt(2)/dy,...
                 0]);                    % y1, y2, y3
N_cnt_z = N_package_z;                             % z1, z2, z3

N_pdms_x = N_package_x;                             % x1, x2, x3
N_pdms_y = int16([N_cnt_y(2), ...
                 N_cnt_y(2) + dim_pdms(2)/dy,...
                 0]);                    % y1, y2, y3
N_pdms_z = N_package_z;                            % z1, z2, z3

%% Presión inicial
% En CNT
N_presure_x = int16([Nx0 - dim_presure(1)/(2*dx),...
                     Nx0 + dim_presure(1)/(2*dx), ...
                     0]);                        % x1, x2, x3
N_presure_y = int16([N_cnt_y(1), ...
                     N_cnt_y(1) + dim_presure(2)/dy + 1,...
                     0]);                    % y1, y2, y3
N_presure_z = int16([Nz0 - dim_presure(3)/(2*dz),...
                     Nz0 + dim_presure(3)/(2*dz), ...
                     0]);                          % z1, z2, z3
% En PDMS

%% Sensor
N_sensor_sensing_x = int16([Nx0 - dim_sensor_sensing(1)/(2*dx),...
                    Nx0 + dim_sensor_sensing(1)/(2*dx),...
                    0]);
N_sensor_sensing_y = int16([Ny-PML_SIZE - 1 - dim_sensor_sensing(2)/dy, Ny-PML_SIZE-1,0]);
N_sensor_sensing_z = int16([Nz0 - dim_sensor_sensing(3)/(2*dz),...
                    Nz0 + dim_sensor_sensing(3)/(2*dz),...
                    0]);                         % z1, z2, z3

N_sensor_conducting_x = int16([Nx0 - dim_sensor_conducting(1)/(2*dx),...
                    Nx0 + dim_sensor_conducting(1)/(2*dx),...
                    0]);
N_sensor_conducting_y = int16([Ny-PML_SIZE-1 - dim_sensor_conducting(2)/dy - ...
                        dim_sensor_sensing(2)/dy, Ny-PML_SIZE-1-dim_sensor_conducting(2)/dy,0]);
N_sensor_conducting_z = int16([Nz0 - dim_sensor_conducting(3)/(2*dz),...
                    Nz0 + dim_sensor_conducting(3)/(2*dz),...
                    0]);                         % z1, z2, z3
%% Posiciones 3D
pos_package = {N_package_x(1):N_package_x(2); ...
                N_package_y(1):N_package_y(2);
                N_package_z(1):N_package_z(2)}; % x_vec, y_vec
pos_emisor = {N_emisor_x(1):N_emisor_x(2); ...
                N_emisor_y(1):N_emisor_y(2);
                N_emisor_z(1):N_emisor_z(2)}; % x_vec, y_vec
pos_separation = {N_separation_x(1):N_separation_x(2); ...
                N_separation_y(1):N_separation_y(2);
                N_separation_z(1):N_separation_z(2)}; % x_vec, y_vec

pos_cnt  = {N_cnt_x(1):N_cnt_x(2); ...
                N_cnt_y(1):N_cnt_y(2);
                 N_cnt_z(1):N_cnt_z(2)}; % x_vec, y_vec
pos_pdms = {N_pdms_x(1):N_pdms_x(2); ...
                N_pdms_y(1):N_pdms_y(2);
                N_pdms_z(1):N_pdms_z(2)}; % x_vec, y_vec

pos_presure = {N_presure_x(1):N_presure_x(2); ...
                N_presure_y(1):N_presure_y(2);
                N_presure_z(1):N_presure_z(2)}; % x_vec, y_vec

pos_sensor_conducting = {N_sensor_conducting_x(1):N_sensor_conducting_x(2); ...
                N_sensor_conducting_y(1):N_sensor_conducting_y(2);
                N_sensor_conducting_z(1):N_sensor_conducting_z(2)}; % x_vec, y_vec
            
pos_sensor_sensing = {N_sensor_sensing_x(1):N_sensor_sensing_x(2); ...
                N_sensor_sensing_y(1):N_sensor_sensing_y(2);
                N_sensor_sensing_z(1):N_sensor_sensing_z(2)}; % x_vec, y_vec