function [] = plot_experiment( sos , pos_cnt, pos_emisor, pos_package, pos_pdms,...
                            	pos_presure, pos_sensor_conducting, pos_sensor_sensing, ...
                                pos_separation, Nx, Ny, Nz, x_vec, y_vec, z_vec )

    [~, scale, prefix] = scaleSI(max([max(x_vec), max(y_vec)]));

    subplot(3,2,[1,3,5])
    voxelPlotM(pos_cnt, pos_emisor, pos_package, pos_pdms,...
                                    pos_presure, pos_sensor_conducting, pos_sensor_sensing,...
                                    pos_separation, Nx, Ny, Nz);

    subplot(3,2,2);
    imagesc((x_vec - min(x_vec))*scale, y_vec*scale, ...
            double(squeeze(sos(:, :, Nz/2))));
    xlabel([prefix,'m']);ylabel([prefix,'m']);title('y-x plane')

    subplot(3,2,4);
    imagesc((x_vec - min(x_vec))*scale, z_vec*scale, ...
            double(squeeze(sos(:, Ny/2, :))));
    xlabel([prefix,'m']);ylabel([prefix,'m']);title('x-z plane')

    subplot(3,2,6);
    imagesc((y_vec - min(y_vec))*scale, z_vec*scale, ...
            double(squeeze(sos(Nx/2, :, :))));
        xlabel([prefix,'m']);ylabel([prefix,'m']);title('x-y plane')


end

