function [matrix] = define_source_2D(option, period, dt, t_array, source, pressure_x_pos, pressure_y_pos)
%DEFINE_SOURCE   Generates a pressure wave
%
% DESCRIPTION:
%       define_source generates a single pressure 2D wave depending on 
%       the option selected and the magnitude and period given
%
% USAGE:
%       source.p = define_source(option, source_mag, period, dt)
%
% INPUTS:
%       option      		 - Wave form to use
%		dt                   - Sampling frequency
%		source_mag           - Amplitude of the wave
%		period               - Duration of the wave   
%       t_array              - Time vector
%       source               - Source matrix where we can find the mask
%       pos_source           - Space where we can find the pressure
%
% OPTIONAL INPUTS:
%		None
%
% OUTPUTS:
%       matrix               - One cycle pressure wave that results from the
%                              arguments given
%

sz = source.p_mask(pressure_x_pos, pressure_y_pos);
a = size(sz); a = a(1);
sz = sz(:);
matrix(1:length(sz), 1:length(t_array)) = 0;
source_freq = 1/period;  
cnt = 1;

source_mag = (source_freq*1e-9);

for i = 1:length(sz)
    if strcmp(option, 'None')
        matrix(i, 1:length(t_array)) = source_mag;%.*exp(-1000.*(double(pressure_y_pos(cnt)-pressure_y_pos(1)))/1000);
    elseif strcmp(option, 'square')
        matrix(i, 1:length(t_array)) = source_mag.*exp(-1000.*(double(pressure_y_pos(cnt)-pressure_y_pos(1)))/1000)*(abs(square(2*pi*source_freq*t_array)) >= 0);
    elseif strcmp(option, 'triangle')
        source_freq = 1/(2*period); 
        temp = sawtooth(2*pi*source_freq*t_array, 0.5);
        temp(temp<0) = 0;
        matrix(i, 1:length(t_array)) = source_mag.*(exp(-1000.*(double(pressure_y_pos(cnt)-pressure_y_pos(1)))/1000)*(temp));
    else
        msg = 'Bad input option at define_source_2D.';
        error(msg)
    end;
    if mod(i, a)==0
        cnt = cnt+1;
    end
end

matrix(:, round(1/(dt*source_freq)):length(matrix)) = 0;

end

