% create the computational grid
Nx = 128;            % number of grid points in the x direction
Ny = 128;            % number of grid points in the y direction
Nz = 128;            % number of grid points in the z direction
dx = 10e-6;        % grid point spacing in the x direction [m]
dy = 10e-6;        % grid point spacing in the y direction [m]
dz = 10e-6;        % grid point spacing in the z direction [m]

x = Nx*dx;
y = Ny*dy;
z = Nz*dz;

kgrid = kWaveGrid(Nx, dx, Ny, dy, Nz, dz);
