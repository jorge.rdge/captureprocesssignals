close all; 
clear all;

%% Import and Order Data
rutaCapturasAcoustic = '.\data\';

dataListDir = ls(rutaCapturasAcoustic);
dataListTmp = dataListDir(3:end-1, :);

cmutSignalsMax = zeros(10, 3400);
for ink = 2

    dataraw = importdata([rutaCapturasAcoustic, dataListTmp(ink, :)]);

    data = dataraw.captura;
    vh30A = [65, 55, 47.5, 45, 45, 42.55, 42.5, 42.5, 42.5, 42.5];
    time_acoustic = data(1).cmut1.t0 + data(1).cmut1.dt*...
                    (0:length(data(1).cmut1.signal) - 1);              

    cmutSignals1 = zeros(8, 15000); cmutSignals2 = zeros(8, 15000);
    cmutSignals3 = zeros(8, 15000); cmutSignals4 = zeros(8, 15000);

    for k = 1:length(data); cmutSignals1(k, :) = data(k).cmut1.signal; end
    for k = 1:length(data); cmutSignals2(k, :) = data(k).cmut2.signal; end
    for k = 1:length(data); cmutSignals3(k, :) = data(k).cmut3.signal; end
    for k = 1:length(data); cmutSignals4(k, :) = data(k).cmut4.signal; end
    cmutSignals = [cmutSignals1;cmutSignals2;cmutSignals3;cmutSignals4];

    cmutSignalsZoom = cmutSignals(:, [6500:9899]);
    time_acousticZoom = time_acoustic([6500:9899]);

% Get Maximum amplitude pulses
    [maxVal, maxIdx(ink)] = max(max(cmutSignalsZoom, [], 2));
    if ink == 1
    	cmutSignalsMax(10,:) = cmutSignalsZoom(maxIdx(ink),:);
    else
        cmutSignalsMax(ink-1,:) = cmutSignalsZoom(maxIdx(ink),:);
    end
end

figure;
for k = 1:10
    plot(cmutSignalsMax(k, :));hold on;
end

CMUTmean = mean(cmutSignals, 1);
plot(CMUTmean([6500:9899])); hold on; plot(cmutSignalsMax(10,:), 'r')

figure;
plot(time_acoustic/1e-6, cmutSignals(25,:))
xlabel('us'); ylabel('V'); title('Typical acoustic signal obtained with the CMUT')

%% Process acoustic 30 A
           
plotOpt = 1;
posWidthList = zeros(size((cmutSignalsMax), 1), 1);
negWidthList = zeros(size((cmutSignalsMax), 1), 1);
posNegWidthList = zeros(size((cmutSignalsMax), 1), 1);
maxVal = zeros(size((cmutSignalsMax), 1), 1);
minVal = zeros(size((cmutSignalsMax), 1), 1);

pulseOpt = 'Acoustic';

dataWidthList = 10:10:100;

for k = 1:length(dataWidthList)
[posWidthList(k), negWidthList(k), posNegWidthList(k), maxVal(k), minVal(k), risetime30A(k)] = ...
    processPulse(time_acousticZoom, ...
                 cmutSignalsMax(k, :), ...
                 1:length(cmutSignalsMax(k, :)), ...
                 plotOpt, 1);
end
plot([10:10:100], risetime30A'/1e-9);hold on;plot([10:10:100], ones(1,10)*mean(risetime30A)/1e-9)

sigList = zeros(10, 3400);
for k = 1:size(sigList,1)
    sigList(k, :) = smooth(normalizado(cmutSignalsMax(k, :)), 5);
end

normOpt = 1; criteria = 0.2;
[alList, idx] = alignData(sigList, criteria, normOpt);

figure;
for k = 1:size(sigList,1)
    plot(time_acousticZoom/1e-6, alList(k, :)); hold on;
end
% 25MHz
% axis([2.31 2.45 min(min(alList30))-0.02 max(max(alList30))+0.02])
% 10MHz
axis([4.3 4.7 min(min(alList))-0.02 max(max(alList))+0.02])
xlabel('us');ylabel('mV'); title('Acoustic pulses CMUT')
legend(num2str(dataWidthList', '%0.2f ns'));
% savefig(gcf, ['.\Figures\AcousticPulses10MHz30A.fig']);
% savefig(gcf, ['.\Figures\AcousticPulses10MHz30ANorm.fig']);


%% FFT acoustic 30A
           
% 10MHz
% idxAcoustic  = 100:700;
% 25MHz
idxAcoustic  = [100:700];% 1:700 for 10MHz

figure;
for k = 1:10
    plot(alList(k,idxAcoustic));
    hold on;
end


Ts = data(1).cmut1.dt;
Fs = 1/Ts; Fn = Fs/2; 
pulseWidthList = 10:10:100;
legendCell = cellstr(num2str(pulseWidthList', '%-d ns'));

bwIdxList = zeros(10, 7);
bwList = zeros(size(bwIdxList));

range = 70e6;

figure;
for k = 1:length(data)
    [fvec, S, bwIdxList(k, :) ] = spec(alList(k,idxAcoustic), Fs, range);
    bwList(k,:) = fvec(bwIdxList(k,:))/1e6;
    
%     figure;
    plot(fvec/1e6, 20*log10(abs(S)./max(abs(S)))); hold on;
%     plot(fvec(bwIdxList30A(k, :))/1e6, [-6, -6, -10, -10,-20, -30, -40], 'r*')
end
axis([0, range/1e6, -70, 2]);
title('Acoustic pulse CMUT FFT');
xlabel('MHz'); ylabel('dB')
line(fvec, linspace(-6, -6, length(fvec)), 'Color', 'k');
line(fvec, linspace(-20, -20, length(fvec)), 'Color', 'k');
line(fvec, linspace(-40, -40, length(fvec)), 'Color', 'k');
line(fvec, linspace(-60, -60, length(fvec)), 'Color', 'k');

legend(legendCell)


%% Display Data

figure;
plot(dataWidthList, posWidthList/1e-9); hold on
plot(dataWidthList, negWidthList/1e-9);
plot(dataWidthList, posNegWidthList/1e-9);
title('Acoustic pulse widths comparison');
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Pulse Width [ns]')
legend({'Positive Lobe', 'Negative Lobe', 'Distance between max and min '})

figure;
plot(pulseWidthList, maxVal/1e-3); hold on
plot(pulseWidthList, abs(minVal)/1e-3); hold on

title('Maximum Amplitudes per input Width');
xlabel('Input pulse Width [ns]');
ylabel('Acoustic Pulse Amplitude [mV]')
legend({'Maximum', 'Minimum'})

% %% save
% save(['.\Procesado\Processed_Data_OSRAMDriverComercial_CMUT'], ...
%         'pulseWidthList', 'maxVal', 'minVal', 'negWidthList', ...
%         'posNegWidthList','bwList')
%     
% save(['.\Procesado\acousticSignals_CMUT_timeacousticCMUT'], 'time_acousticZoom')
% 

%% ORDER and DISPLAY data
res1 = [1:16]; res2 = [32:-1:15];
c = 1;
for k = 1:16
   res(c) = res1(k); 
   res(c+1) = res2(k); 
   c=c+2;
end

for k = 1:32
    [maxVal(k), maxIdx(k)] = max(cmutSignalsZoom(k,:));
end

[a,b] = sort(time_acousticZoom(maxIdx), 'descend');
c = 1;
for k = b
    subplot(32, 1, res(c))
    plot(time_acousticZoom, cmutSignalsZoom(k,:))
    c = c+1;
end



