function [ alignSigList, idx1 ] = alignData( sigList, criteria, normOpt)
%ALIGNDATA Summary of this function goes here
%   Detailed explanation goes here
    N = size(sigList,1);

    maxs = max(sigList, [], 2);   %maxss = 1;%max(maxs) ;
    for k = 1:N
        if normOpt
%             sigListNorm(k, :) = ((sigList(k,:) - min(sigList(k,:))) / ( max(sigList(k,:))...
%            - min(sigList(k,:))));
           sigListNorm(k, :)= normalizado(sigList(k,:));
        else
            sigListNorm(k, :) = sigList(k,:);
        end
        %(sigList(k,:) - min(sigList(k,:))) / ( max(sigList(k,:))...
        %    - min(sigList(k,:)) );
        %sigList(k,:)/norm(sigList(k,:));
    end

    maxsNorm = max(sigListNorm, [], 2);
    [~, maxSignIdx] = max(maxsNorm);
    for k = 1:N
        sigtmp = sigListNorm(k,:);
        [~, idxMax] = max(sigtmp);
        for c = idxMax:-1:1
            if sigtmp(c) <= criteria
                idxRefList(k) = c;
                break;
            end
        end
    end
    
    idxRef = max(idxRefList);
    
    alignSigList =  zeros(10, size(sigList, 2));
    
    for k = 1:N
        sigtmp = sigListNorm(k,:);
        [~, idxMax] = max(sigtmp);
%         [~, idx(k)] = min(abs(sigtmp(1:idxMax)-refVal));
        for c = idxMax:-1:1
            if sigtmp(c) <= criteria
                idx1(k) = c;
                break;
            end
        end
        alignSigList(k, :) = circshift(sigtmp', [abs(idxRef-idx1(k)), 0]);
    end
end

